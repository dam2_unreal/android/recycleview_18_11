package com.example.recycleview_18_11;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PersonajeAdapter.ListClickItem{

    private RecyclerView recView;
    private ArrayList<Personaje> miLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recView = findViewById(R.id.recView);
        miLista = new ArrayList<>();

        recView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        PersonajeAdapter adapter = new PersonajeAdapter(miLista, this);
        recView.setAdapter(adapter);

        cargarPersonajes();

    }

    private void cargarPersonajes() {
        Personaje bart = new Personaje("Bart","El niño mas travieso de la serie", R.drawable.bart);
        Personaje burns = new Personaje("Burns","El dueño de la central nuclear", R.drawable.burns);
        Personaje carl = new Personaje("Carl","Amigo y compañero de trabajo de Homer", R.drawable.carl);
        Personaje edna = new Personaje("Edna","Profesora del instituto", R.drawable.edna);
        Personaje fattoni = new Personaje("Fattoni","Lider de la mafia italiana", R.drawable.fattoni);
        Personaje homer = new Personaje("Homer","Protagonista", R.drawable.homer);
        Personaje krusty = new Personaje("Krusty","Payaso de television", R.drawable.krusty);
        Personaje lenny = new Personaje("Lenny","Amigo y compañero de trabajo de Homer", R.drawable.lenny);
        Personaje lisa = new Personaje("Lisa","La niña mas lista de la serie", R.drawable.lisa);
        Personaje milhouse = new Personaje("Milhouse","Amigo de Bart", R.drawable.milhouse);
        Personaje moe = new Personaje("Moe","Dueño de la taberna Moe", R.drawable.moe);
        Personaje nelson = new Personaje("Nelson","Abuson del instituto", R.drawable.nelson);

        miLista.add(bart);
        miLista.add(burns);
        miLista.add(carl);
        miLista.add(edna);
        miLista.add(fattoni);
        miLista.add(homer);
        miLista.add(krusty);
        miLista.add(lenny);
        miLista.add(lisa);
        miLista.add(milhouse);
        miLista.add(moe);
        miLista.add(nelson);
    }

    @Override
    public void onListClickItem(int posicion)
    {
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setMessage(miLista.get(posicion).getNombre()+", "+miLista.get(posicion).getDescripcion());
        msg.setPositiveButton("Aceptar", null);
        msg.show();
    }
}
