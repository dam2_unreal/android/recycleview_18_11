package com.example.recycleview_18_11;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class PersonajeAdapter extends RecyclerView.Adapter<PersonajeAdapter.MiVistaHolder>
{

    ArrayList<Personaje> personajes;

    //Paso 2: ponemos un listener de la interfaz como atributo
    final ListClickItem myOnListClickItemListener;

    //Paso 5: le pasamos al constructor el ListClickItemn
    public PersonajeAdapter(ArrayList<Personaje> personajes, ListClickItem list)
    {
        myOnListClickItemListener = list;
        this.personajes = personajes;
    }

    @NonNull
    @Override
    //Aqui nos traeriamos el layout (lo inflamos es decir hacemos que se pinte)
    public PersonajeAdapter.MiVistaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.lista_personaje, parent, false);
        return new MiVistaHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonajeAdapter.MiVistaHolder holder, int position)
    {
        holder.nombre.setText(personajes.get(position).getNombre());
        holder.descripcion.setText(personajes.get(position).getDescripcion());
        holder.logo.setImageResource(personajes.get(position).getImg());
    }

    @Override
    public int getItemCount() {
        return personajes.size();
    }

    //Esta clase inicializa los elementos del layout (seria el cargarViews que solemos hacer)
    //Paso 3 Implemennto view.onclicklistener
    public class MiVistaHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView nombre, descripcion;
        private ImageView logo;

        public MiVistaHolder(@NonNull View itemView)
        {
            super(itemView);
            nombre = itemView.findViewById(R.id.tvNombre);
            descripcion = itemView.findViewById(R.id.tvDescripcion);
            logo = itemView.findViewById(R.id.ivLogo);
            //Paso4: añado al listener
            itemView.setOnClickListener(this);
        }

        public void onClick(View v)
        {
            int p = getAdapterPosition();
            myOnListClickItemListener.onListClickItem(p);
        }

    }

    //Paso 1: Implemtamos interfaz
    public interface ListClickItem
    {
        void onListClickItem(int posicion);
    }

}