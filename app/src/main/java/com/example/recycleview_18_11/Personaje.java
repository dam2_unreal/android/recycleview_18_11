package com.example.recycleview_18_11;

public class Personaje {
    private String nombre, descripcion;
    private int img;

    public Personaje(String nombre, String descripcion, int img) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.img = img;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getImg() {
        return img;
    }
}
